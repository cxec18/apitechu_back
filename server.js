var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
const URL_BASE = "/techu-peru/v1/";

app.use(bodyParser.json());

app.get(URL_BASE, function (req, res) {
res.status(999);
  res.send({"msg":"Operacion get exitosa"});
});


app.get(URL_BASE + 'users', function (req, res) {
  //res.send({"msg":"Operacion get exitosa"});
  res.send(user_file);
});

/*
app.get(URL_BASE + 'users/:id', function (req, res) {
  let pos = req.params.id -1;
  console.log("GET con id = " + req.params.id);
  res.send(user_file[pos]);
});

app.post(URL_BASE + 'users', function (req, res) {
  console.log("POST de users");
  let newId = user_file.length + 1;
  let newUser = {
    "id" : newId,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  }
  user_file.push(newUser);
  console.log("nuevo usuario: " + newUser);
  console.log("nuevo usuario.id: " + newUser.id);
  res.send(newUser);
});

app.put(URL_BASE + 'users/:id',
function(req, res){
  console.log('PUT de users');
  let newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
    }
  user_file[req.params.id - 1] = newUser;
  console.log("usuario modificado: " + newUser);
  res.send(newUser);
});

app.delete(URL_BASE + 'users/:id',
function(req, res){
  console.log('Delete de users.');
  let pos = req.params.id;
  user_file.splice(pos - 1, 1);
  res.send({"msg":"usuario eliminado"});
});
*/

var usersLogged = [];

// LOGIN
app.post(URL_BASE + 'users/login', function (req, res) {
  console.log("POST login de users");

  let loginUser = {
    "email": req.body.email,
    "password": req.body.password
  }

  var i=0;
  var encontrado=false;
  var userChecked;

  while(i<user_file.length && !encontrado){
    userChecked = user_file[i];
    if(userChecked.email == loginUser.email &&
       userChecked.password == loginUser.password){
       console.log("usuario encontrado: " + userChecked.id);
       encontrado = true;
       userChecked.logged = true;
       usersLogged.push(userChecked);
    }
    i++;
  }
  if(!encontrado){
    userChecked = {"descripcion": "Usuario incorrecto"}
    res.status(403);
  }

  res.send(userChecked);
});

//LIST USERS
app.get(URL_BASE + 'users/logged', function (req, res) {
  console.log("GET usuarios logged");
  console.log("Numero de logeados= " + usersLogged.length);
  res.send(usersLogged);
});

// LOGOUT
app.post(URL_BASE + 'users/logout/:id', function (req, res) {
  console.log("POST logout de users");
  var i=0;
  var encontrado=false;

  while(i<usersLogged.length && !encontrado){
    userChecked = usersLogged[i];
    if(userChecked.id == req.params.id){
       console.log("usuario encontrado: " + req.params.id);
       encontrado = true;
       userChecked.logged = false;
       usersLogged.splice(i, 1);
       res.status(200);
    }
    i++;
  }

  if(!encontrado){
    userChecked = {"descripcion": "Usuario no logeado"}
    res.status(400);
  }
  res.send(userChecked);
});

app.listen(port, function (req, res) {
  console.log('Example app listening on port 3000!');
});
