var express = require('express');
var requestJSON = require('request-json');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
require('dotenv').config();

var app = express();
var port = process.env.PORT || 3000;
const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu36db/collections/';
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const URL_BASE = process.env.URL_BASE;
const apikeyMLab = 'apiKey=' + process.env.APIKEY_MLAB;

app.use(bodyParser.json());

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users',
 function(req, res) {
   console.log("Cliente GET mLab");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id/accounts',
 function(req, res) {
   console.log("Cliente GET user's accounts = " + req.params.id);

   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&q={"idUsuario":' + req.params.id + '}&';

   httpClient.get('account?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'account'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// POST users consumiendo API REST de mLab
app.post(URL_BASE + 'users', function(req, res) {
   console.log("POST de users - body: " + req.body);

   var clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('user?' + apikeyMLab, function(error, respuestaMLab, body){

     let newId = body.length + 1;
     console.log("newId: " + newId);

     let newUser = {
       "id" : newId,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
       "password" : req.body.password
     }

     clienteMlab.post('user?' + apikeyMLab, newUser, function(error, respuestaMLab, body){
       console.log("body (post): " + body);
       res.send(body);
     });

   });

});

//PUT users con parámetro 'id'
/*
app.put(URLbase + 'users1/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});*/

// Petición PUT con id de mLab (_id.$oid)
/*
 app.put(URLbase + 'users2/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });
*/

app.listen(port, function (req, res) {
  console.log('Example app listening on port 3000!');
});
